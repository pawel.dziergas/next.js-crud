# Simple CRUD app

CRUD app based on Next.js with TypeScript, React Hooks, Redux, Redux Saga, Formik, React-testing-library/Jest.

## Useful commands

Run app locally:

```
npm i
npm run dev
```

To run backend mocks:

``
npm run mock-server
``

For generate test coverage report

``
npm run test
``

Credentials for login are available in ``.env`` file

