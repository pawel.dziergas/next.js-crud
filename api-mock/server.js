require('dotenv').config();
const jsonServer = require('json-server');
const bodyParser = require('body-parser');
const server = jsonServer.create();
const router = jsonServer.router('api-mock/database.json');
const middlewares = jsonServer.defaults();
const SERVER_PORT = 3001;
const API_URL = '/api/';
const jsonParser = bodyParser.json();
const jwt = require('jsonwebtoken');

server.use(middlewares);

// server.use((req, res, next) => {
//   if (req.method === 'GET' && req.query && req.query.page) {
//     req.query._page = req.query.page;
//     req.query._limit = req.query.perPage;
//   }
//   next();
// });

server.post(`${API_URL}login`, jsonParser, (req, res) => {
  const { email, password } = req.body;
  const { USER_LOGIN, USER_PASS } = process.env;

  if (email === USER_LOGIN && password === USER_PASS) {
    const accessToken = jwt.sign({ foo: 'bar' }, 'shhhhh');
    return res.status(201).jsonp({
      accessToken
    });
  }

  return res.status(401).jsonp({
    message: 'Incorrect login or password'
  });
});

server.use(API_URL, router);
server.listen(SERVER_PORT, () => {
  console.log('JSON Server is running on port: ', SERVER_PORT);
});
