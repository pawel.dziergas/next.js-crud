import nookies from 'nookies';

const defaultMaxAge = 30 * 24 * 60 * 60;

export const getCookie = (key: string, ctx?: any) => {
  const cookie = nookies.get(ctx);
  return cookie[key];
};

export const setCookie = (key: string, value: string, maxAge: number = defaultMaxAge, ctx?: any) => {
  // @ToDo - resolve issue with httpOnly flag
  nookies.set(ctx, key, value, { maxAge: maxAge, path: '/' });
};

export const destroyCookie = (key: string, ctx?: any) => {
  nookies.destroy(ctx, key);
};
