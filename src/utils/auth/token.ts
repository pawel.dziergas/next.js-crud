import jwtDecode from 'jwt-decode';

import { getCookie } from './cookies';

export const isLoggedIn = (ctx?: any) => {
  const token = getCookie('token', ctx);
  let decodedToken: any;
  if (token) {
    try {
      decodedToken = jwtDecode(token);
    } catch (error) {
      return false;
    }
  } else {
    return false;
  }

  const isExpired = new Date() > new Date(decodedToken.exp * 1000);

  return !isExpired;
};
