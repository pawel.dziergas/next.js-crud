import axios from 'axios';

import { API_URL } from '../../config';
import { getCookie } from '../auth/cookies';

const onRequest = (config: any, ctx?: any) => {
  const token = getCookie('token', ctx);
  if (token) {
    config.headers = { ...config.headers, Authorization: `Bearer ${token}` };
  }
  return Promise.resolve(config);
};

const apiClient = (ctx?: any) => {
  const instance = axios.create({
    baseURL: API_URL
  });

  instance.interceptors.request.use(config => onRequest(config, ctx));
  return instance;
};

export default apiClient;
