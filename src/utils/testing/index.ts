import { fireEvent } from '@testing-library/react';

export function changeInputValue(input: HTMLElement, value: string) {
  return fireEvent.change(input, {
    target: {
      value
    }
  });
}
