import App from 'next/app';
import React, { ReactNode } from 'react';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import createStore from '../store';
import '../public/styles/index.scss';
import { ThemeProvider } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#ffffff',
      main: '#4b7bec'
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      light: '#0066ff',
      main: '#ffffff',
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#7F8C8D'
    }
  }
});

interface MyAppState {
  Component: ReactNode;
  pageProps: any;
  store: any;
}

class MyApp extends App<MyAppState> {
  static async getInitialProps({ Component, ctx }: any) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Component {...pageProps} />
          <ToastContainer autoClose={2000} pauseOnFocusLoss={false} hideProgressBar className="toast-container" />
        </Provider>
      </ThemeProvider>
    );
  }
}

export default withRedux(createStore)(withReduxSaga(MyApp));
