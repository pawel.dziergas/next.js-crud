import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { Formik, Field, Form, FormikHelpers } from 'formik';
import { isLoginRequestingSelector } from '../../store/modules/Login/selectors';
import Layout from '../../components/Layout';
import { login } from '../../store/modules/Login/actions';
import { ILoginData } from '../../store/modules/Login/types/models';
import Input from '../../components/Inputs/InputForm';
import SubmitBtn from '../../components/Buttons/Submit';
import Card from '../../components/Card';
import validationSchema from './validationSchema';

const initialValues: ILoginData = {
  email: '',
  password: ''
};

const Login = () => {
  const dispatch = useDispatch();
  const requesting = useSelector(isLoginRequestingSelector);

  const onSubmit = async (values: ILoginData, actions: FormikHelpers<ILoginData>) => {
    actions.setStatus({ error: null });
    await dispatch(login({ values, actions }));
  };

  return (
    <Layout title="Login">
      <div className="page">
        <Card width="450px">
          <Formik onSubmit={onSubmit} validationSchema={validationSchema} initialValues={initialValues}>
            {({ isValid, dirty, status }) => (
              <Form>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Field required name="email" label={'Email'} component={Input} />
                  </Grid>
                  <Grid item xs={12}>
                    <Field required name="password" type="password" label={'Password'} component={Input} />
                  </Grid>
                  <Grid item xs={12}>
                    <SubmitBtn disabled={requesting || !isValid || !dirty} text={'Login'} />
                  </Grid>
                </Grid>
                {status?.error && (
                  <p className="error-message error-message--modal" data-testid="login-form-status-message">
                    {status.error}
                  </p>
                )}
              </Form>
            )}
          </Formik>
        </Card>
      </div>
    </Layout>
  );
};

export default Login;
