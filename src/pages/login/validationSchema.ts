import * as yup from 'yup';
import { ILoginData } from '../../store/modules/Login/types/models';

const validationSchema = yup.object().shape<ILoginData>({
  email: yup
    .string()
    .email()
    .required()
    .label('Email'),
  password: yup
    .string()
    .required()
    .label('Password')
});

export default validationSchema;
