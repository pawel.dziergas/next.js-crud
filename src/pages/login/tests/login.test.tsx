import React from 'react';
import { render, RenderResult, fireEvent, waitFor, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Login from '../';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { initialState } from '../../../store/modules/Login/reducers';
import { changeInputValue } from '../../../utils/testing';

function fillForm({ getByTestId }: RenderResult) {
  const email = getByTestId('email-input');
  const password = getByTestId('password-input');
  changeInputValue(email, 'example@example.test.cz');
  changeInputValue(password, 'password123');
  fireEvent.blur(password);
}

describe('Login', () => {
  let view: RenderResult;
  let store;
  const mockStore = configureStore();

  beforeEach(() => {
    store = mockStore({
      login: initialState
    });
    view = render(
      <Provider store={store}>
        <Login />
      </Provider>
    );
  });

  test('submit button should be initially disabled', () => {
    const { getByRole } = view;

    expect(getByRole('button')).toHaveAttribute('disabled');
  });

  test('should show validation on input blur', async () => {
    const { getByTestId } = view;

    const input = getByTestId('email-input');
    fireEvent.blur(input);

    await waitFor(() => {
      const validationError = getByTestId('input-validation-error');
      expect(validationError).not.toBe(null);
      expect(validationError).toHaveTextContent('Email is a required field');
    });
  });

  test('should get validation error for incorrect e-mail format', async () => {
    const { getByTestId } = view;

    const input = getByTestId('email-input');
    changeInputValue(input, 'example@');
    fireEvent.blur(input);

    await waitFor(() => {
      const validationError = getByTestId('input-validation-error');
      expect(validationError).not.toBe(null);
      expect(validationError).toHaveTextContent('Email must be a valid email');
    });
  });

  test('for already fulfilled form submit button should be clickable', async () => {
    const { getByRole } = view;
    fillForm(view);

    await waitFor(() => {
      expect(getByRole('button')).not.toHaveAttribute('disabled');
    });
  });
});
