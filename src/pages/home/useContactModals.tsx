import React, { useState } from 'react';
import { contactAdd, contactEdit, contactRemove } from '../../store/modules/Contacts/actions';
import { ContactDataFormModal, ContactRemoveModal } from '../../components/Modals';
import { useDispatch, useSelector } from 'react-redux';
import { Nullable } from '../../types/nullable';
import { getContactById } from '../../store/modules/Contacts/selectors';

export enum modalTypes {
  'ADD' = 'ADD',
  'EDIT' = 'EDIT',
  'REMOVE' = 'REMOVE'
}

export type modalType = modalTypes.ADD | modalTypes.EDIT | modalTypes.REMOVE;

const useContactModals = () => {
  const dispatch = useDispatch();
  const [id, setId] = useState<string>('');
  const [modalName, setModalName] = useState<Nullable<modalType>>(null);
  const editedItem = useSelector(getContactById(id));

  const openModal = (type: modalType, id?: string) => {
    if (id) {
      setId(id);
    }

    return setModalName(type);
  };

  const closeModal = () => {
    setId('');
    return setModalName(null);
  };

  const modal = () => {
    switch (modalName) {
      case modalTypes.ADD: {
        return (
          <ContactDataFormModal
            handleClose={() => closeModal()}
            header="Add new contact"
            text="Fill all required fields to add new item."
            onSubmit={(values, actions) => {
              dispatch(contactAdd({ values, actions }));
            }}
          />
        );
      }
      case modalTypes.EDIT: {
        return (
          <ContactDataFormModal
            handleClose={() => closeModal()}
            header={`Edit contact ${editedItem?.name}`}
            text="Change fields which you want to edit."
            initialValues={editedItem}
            onSubmit={(values, actions) => {
              dispatch(contactEdit(id, { values, actions }));
            }}
            submitText="Edit"
          />
        );
      }
      case modalTypes.REMOVE: {
        return (
          <ContactRemoveModal
            handleClose={() => closeModal()}
            text={`Are you sure to remove contact: ${editedItem?.name}?`}
            onSubmit={() => {
              dispatch(contactRemove(id));
              closeModal();
            }}
          />
        );
      }
      default: {
        return null;
      }
    }
  };

  return {
    openModal,
    modal
  };
};

export default useContactModals;
