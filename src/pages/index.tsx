import React, { useState } from 'react';
import Layout from '../components/Layout';
import { useSelector } from 'react-redux';
import { getContactsListSelector, getContactsRequestingSelector } from '../store/modules/Contacts/selectors';
import './home/styles.scss';
import { contactsFetch } from '../store/modules/Contacts/actions';
import { Context } from '../types/ctx';
import { Container, Grid } from '@material-ui/core';
import SearchBar from '../components/Searchbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import Loader from '../components/Loader';
import withAuth from '../components/HOC/withAuth';
import ContactList from '../components/ContactList';
import Button from '@material-ui/core/Button';
import PostAddOutlinedIcon from '@material-ui/icons/PostAddOutlined';
import useContactModals, { modalTypes } from './home/useContactModals';

const IndexPage = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const contacts = useSelector(getContactsListSelector(searchQuery));
  const requesting = useSelector(getContactsRequestingSelector);
  const { openModal, modal } = useContactModals();

  return (
    <Layout title="Home" withContainer={false}>
      <section className="home-page__search-section">
        {requesting && (
          <div className="home-page__loading">
            <Loader />
          </div>
        )}
        <SearchBar handleSearch={(input: string) => setSearchQuery(input)} placeholder={'Search by contact name...'} />
      </section>
      <Container>
        <section className="home-page__list-section">
          <div className="home-page__header">
            <h3 className="home-page__section-header">Contact list</h3>
            <Button
              variant="contained"
              startIcon={<PostAddOutlinedIcon />}
              color="primary"
              onClick={() => openModal(modalTypes.ADD)}
            >
              Add new
            </Button>
          </div>
          {requesting ? (
            <div className="home-page__loading-spinner">
              <CircularProgress size={60} />
            </div>
          ) : contacts.length ? (
            <>
              <Grid container spacing={3}>
                <ContactList
                  items={contacts}
                  actionCall={(id, actionType) => {
                    openModal(actionType, id);
                  }}
                />
              </Grid>
            </>
          ) : (
            !requesting && <Alert severity="info">No contacts</Alert>
          )}
        </section>
        {modal()}
      </Container>
    </Layout>
  );
};

IndexPage.getInitialProps = async ({ ctx }: Context) => {
  await ctx.store.dispatch(contactsFetch());
};

export default withAuth(IndexPage);
