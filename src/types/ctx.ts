import { NextPageContext } from 'next';
import { AppContext } from 'next/app';
import { Store } from 'redux';

export interface CustomCtx extends NextPageContext {
  store: Store;
  query: any;
}

export interface Context extends AppContext {
  ctx: CustomCtx;
}
