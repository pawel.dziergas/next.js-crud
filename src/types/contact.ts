export interface IContact {
  id: string;
  name: string;
  email: string;
  country: string;
  phone: string;
}
