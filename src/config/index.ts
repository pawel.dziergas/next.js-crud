export const BASE_HOST_URL = process.env.BASE_HOST_URL;
export const API_URL = `${process.env.API_HOST_URL}/api`;

export const Endpoints = {
  login: '/login',
  contacts: '/contacts'
};

export const Routes = {
  HOME: '/',
  LOGIN: '/login'
};

export const GeneralSettings = {
  title: {
    prefix: 'Contacts app',
    separator: ' | '
  }
};
