import apiClient from '../../../utils/api/apiClient';
import { Endpoints } from '../../../config';
import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from './actions';
import { ActionTypes, IContactsAddRequest, IContactsEditRequest, IContactsRemoveRequest } from './types/actions';
import { v4 as uuidv4 } from 'uuid';
import { IContact } from '../../../types/contact';

const fetchContactsRequest = async (): Promise<{}> => {
  const { data } = await apiClient().get(Endpoints.contacts);

  return data;
};

function* fetchContactsSaga() {
  try {
    const data = yield call(fetchContactsRequest);

    yield put(actions.contactsFetchSuccess(data));
  } catch (err) {
    yield put(actions.contactsFetchError());
  }
}

const createContactRequest = async (formData: IContact): Promise<{}> => {
  const { data } = await apiClient().post(Endpoints.contacts, formData);

  return data;
};

const editContactRequest = async (formData: IContact): Promise<{}> => {
  const { data } = await apiClient().put(`${Endpoints.contacts}/${formData.id}`, formData);

  return data;
};

const deleteContactRequest = async (id: string): Promise<{}> => {
  const { data } = await apiClient().delete(`${Endpoints.contacts}/${id}`);

  return data;
};

function* addContactSaga(action: IContactsAddRequest) {
  try {
    const { actions: formActions } = action.payload;
    const newItem = {
      id: uuidv4(),
      ...action.payload.values
    };

    yield call(createContactRequest, newItem);
    yield put(actions.contactAddSuccess(newItem));
    formActions.resetForm();
    formActions.setStatus({
      success: 'Contact was successfully added.'
    });
  } catch {
    yield put(actions.contactAddError());
  }
}

function* editContactSaga(action: IContactsEditRequest) {
  try {
    const {
      form: { actions: formActions, values },
      id
    } = action.payload;
    const item = { id, ...values };

    yield call(editContactRequest, item);
    yield put(actions.contactEditSuccess(item));
    formActions.resetForm();
    formActions.setStatus({
      success: 'Contact was successfully updated.'
    });
  } catch {
    yield put(actions.contactEditError());
  }
}

function* removeContactSaga(action: IContactsRemoveRequest) {
  try {
    const { id } = action.payload;
    yield call(deleteContactRequest, id);
    yield put(actions.contactRemoveSuccess(id));
  } catch {
    yield put(actions.contactRemoveError());
  }
}

export default function* saga() {
  yield takeLatest(ActionTypes.CONTACTS_FETCH, fetchContactsSaga);
  yield takeLatest(ActionTypes.CONTACTS_ADD, addContactSaga);
  yield takeLatest(ActionTypes.CONTACTS_EDIT, editContactSaga);
  yield takeLatest(ActionTypes.CONTACTS_REMOVE, removeContactSaga);
}
