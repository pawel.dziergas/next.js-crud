import {
  ActionTypes,
  IContactsAddError,
  IContactsAddRequest,
  IContactsAddSuccess,
  IContactsEditError,
  IContactsEditRequest,
  IContactsEditSuccess,
  IContactsFetchError,
  IContactsFetchRequest,
  IContactsFetchSuccess,
  IContactsRemoveRequest,
  IContactsRemoveSuccess,
  IContactsRemoveError
} from './types/actions';
import { IContact } from '../../../types/contact';
import { IContactFormPayload } from '../../../components/ContactDataForm/types';

export function contactsFetch(): IContactsFetchRequest {
  return {
    type: ActionTypes.CONTACTS_FETCH
  };
}

export function contactsFetchSuccess(payload: IContact[]): IContactsFetchSuccess {
  return {
    type: ActionTypes.CONTACTS_FETCH_SUCCESS,
    payload
  };
}

export function contactsFetchError(): IContactsFetchError {
  return {
    type: ActionTypes.CONTACTS_FETCH_ERROR
  };
}

export function contactAdd(payload: IContactFormPayload): IContactsAddRequest {
  return {
    type: ActionTypes.CONTACTS_ADD,
    payload
  };
}

export function contactAddSuccess(payload: IContact): IContactsAddSuccess {
  return {
    type: ActionTypes.CONTACTS_ADD_SUCCESS,
    payload
  };
}

export function contactAddError(): IContactsAddError {
  return {
    type: ActionTypes.CONTACTS_ADD_ERROR
  };
}

export function contactEdit(id: string, form: IContactFormPayload): IContactsEditRequest {
  return {
    type: ActionTypes.CONTACTS_EDIT,
    payload: {
      id,
      form
    }
  };
}

export function contactEditSuccess(payload: IContact): IContactsEditSuccess {
  return {
    type: ActionTypes.CONTACTS_EDIT_SUCCESS,
    payload
  };
}

export function contactEditError(): IContactsEditError {
  return {
    type: ActionTypes.CONTACTS_EDIT_ERROR
  };
}

export function contactRemove(id: string): IContactsRemoveRequest {
  return {
    type: ActionTypes.CONTACTS_REMOVE,
    payload: {
      id
    }
  };
}

export function contactRemoveSuccess(id: string): IContactsRemoveSuccess {
  return {
    type: ActionTypes.CONTACTS_REMOVE_SUCCESS,
    payload: {
      id
    }
  };
}

export function contactRemoveError(): IContactsRemoveError {
  return {
    type: ActionTypes.CONTACTS_REMOVE_ERROR
  };
}
