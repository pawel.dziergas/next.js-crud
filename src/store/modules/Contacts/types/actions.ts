import { IContact } from '../../../../types/contact';
import { IContactFormPayload } from '../../../../components/ContactDataForm/types';

export enum ActionTypes {
  CONTACTS_FETCH = '[CONTACTS] fetch',
  CONTACTS_FETCH_SUCCESS = '[CONTACTS] fetch - success',
  CONTACTS_FETCH_ERROR = '[CONTACTS] fetch - error',
  CONTACTS_ADD = '[CONTACTS] add',
  CONTACTS_ADD_SUCCESS = '[CONTACTS] add - success',
  CONTACTS_ADD_ERROR = '[CONTACTS] add - error',
  CONTACTS_EDIT = '[CONTACTS] edit',
  CONTACTS_EDIT_SUCCESS = '[CONTACTS] edit - success',
  CONTACTS_EDIT_ERROR = '[CONTACTS] edit - error',
  CONTACTS_REMOVE = '[CONTACTS] remove',
  CONTACTS_REMOVE_SUCCESS = '[CONTACTS] remove - success',
  CONTACTS_REMOVE_ERROR = '[CONTACTS] remove - error'
}

export type Actions =
  | IContactsFetchRequest
  | IContactsFetchSuccess
  | IContactsFetchError
  | IContactsAddRequest
  | IContactsAddSuccess
  | IContactsAddError
  | IContactsEditRequest
  | IContactsEditSuccess
  | IContactsEditError
  | IContactsRemoveRequest
  | IContactsRemoveSuccess
  | IContactsRemoveError;

export interface IContactsFetchRequest {
  readonly type: ActionTypes.CONTACTS_FETCH;
}

export interface IContactsFetchSuccess {
  readonly type: ActionTypes.CONTACTS_FETCH_SUCCESS;
  payload: IContact[];
}

export interface IContactsFetchError {
  readonly type: ActionTypes.CONTACTS_FETCH_ERROR;
}

export interface IContactsAddRequest {
  readonly type: ActionTypes.CONTACTS_ADD;
  payload: IContactFormPayload;
}

export interface IContactsAddSuccess {
  readonly type: ActionTypes.CONTACTS_ADD_SUCCESS;
  payload: IContact;
}

export interface IContactsAddError {
  readonly type: ActionTypes.CONTACTS_ADD_ERROR;
}

export interface IContactsEditRequest {
  readonly type: ActionTypes.CONTACTS_EDIT;
  payload: {
    id: string;
    form: IContactFormPayload;
  };
}

export interface IContactsEditSuccess {
  readonly type: ActionTypes.CONTACTS_EDIT_SUCCESS;
  payload: IContact;
}

export interface IContactsEditError {
  readonly type: ActionTypes.CONTACTS_EDIT_ERROR;
}

export interface IContactsRemoveRequest {
  readonly type: ActionTypes.CONTACTS_REMOVE;
  payload: {
    id: string;
  };
}

export interface IContactsRemoveSuccess {
  readonly type: ActionTypes.CONTACTS_REMOVE_SUCCESS;
  payload: {
    id: string;
  };
}

export interface IContactsRemoveError {
  readonly type: ActionTypes.CONTACTS_REMOVE_ERROR;
}
