import { IContact } from '../../../types/contact';

export interface IContactsState {
  list: IContact[];
  requesting: boolean;
  error: boolean;
}
