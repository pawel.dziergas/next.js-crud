import { IApplicationState } from '../../types';
import { createSelector } from 'reselect';

const contactsSelector = (state: IApplicationState) => state.contacts;

export const getContactsListSelector = (searchQuery: string) =>
  createSelector(contactsSelector, contacts =>
    contacts.list.filter(i => i.name.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1)
  );

export const getContactsRequestingSelector = createSelector(contactsSelector, contacts => contacts.requesting);
export const getContactsErrorSelector = createSelector(contactsSelector, contacts => contacts.error);
export const getContactById = (id: string) =>
  createSelector(contactsSelector, contacts => contacts.list.find(i => i.id === id));
