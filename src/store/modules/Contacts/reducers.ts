import { combineReducers } from 'redux';

import { Actions, ActionTypes } from './types/actions';
import { IContactsState } from './types/state';
import { IContact } from '../../../types/contact';

const initialState: IContactsState = {
  list: [],
  requesting: true,
  error: false
};

function list(state = initialState.list, action: Actions): IContact[] {
  switch (action.type) {
    case ActionTypes.CONTACTS_FETCH_SUCCESS: {
      return action.payload;
    }
    case ActionTypes.CONTACTS_FETCH_ERROR: {
      return [];
    }
    case ActionTypes.CONTACTS_ADD_SUCCESS: {
      return [...state, action.payload];
    }
    case ActionTypes.CONTACTS_EDIT_SUCCESS: {
      return [...state.filter(item => item.id !== action.payload.id), action.payload];
    }
    case ActionTypes.CONTACTS_REMOVE_SUCCESS: {
      return [...state.filter(item => item.id !== action.payload.id)];
    }
    default: {
      return state;
    }
  }
}

function requesting(state = initialState.requesting, action: Actions): boolean {
  switch (action.type) {
    case ActionTypes.CONTACTS_FETCH:
    case ActionTypes.CONTACTS_ADD:
    case ActionTypes.CONTACTS_EDIT:
    case ActionTypes.CONTACTS_REMOVE: {
      return true;
    }
    case ActionTypes.CONTACTS_FETCH_SUCCESS:
    case ActionTypes.CONTACTS_FETCH_ERROR:
    case ActionTypes.CONTACTS_ADD_SUCCESS:
    case ActionTypes.CONTACTS_ADD_ERROR:
    case ActionTypes.CONTACTS_EDIT_SUCCESS:
    case ActionTypes.CONTACTS_EDIT_ERROR:
    case ActionTypes.CONTACTS_REMOVE_SUCCESS:
    case ActionTypes.CONTACTS_REMOVE_ERROR: {
      return false;
    }
    default: {
      return state;
    }
  }
}

function error(state = initialState.error, action: Actions): boolean {
  switch (action.type) {
    case ActionTypes.CONTACTS_FETCH_ERROR: {
      return true;
    }
    case ActionTypes.CONTACTS_FETCH_SUCCESS: {
      return false;
    }
    default: {
      return state;
    }
  }
}

export default combineReducers<IContactsState, Actions>({
  list,
  requesting,
  error
});
