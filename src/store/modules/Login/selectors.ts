import { createSelector } from 'reselect';

import { IApplicationState } from '../../../store/types';

const loginSelector = (state: IApplicationState) => state.login;

export const isLoginRequestingSelector = createSelector(loginSelector, login => login.requesting);
export const isLoginErrorOccuredSelector = createSelector(loginSelector, login => login.error);
export const getloggedUserEmail = createSelector(loginSelector, login => login.userEmail);
