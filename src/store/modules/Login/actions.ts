import { ILoginPayload, ILoginSuccessPayload } from './types/models';
import { ActionTypes, ILoginRequest, ILoginSuccess, ILoginError } from './types/actions';

export function login(payload: ILoginPayload): ILoginRequest {
  return {
    type: ActionTypes.LOGIN,
    payload
  };
}

export function loginSuccess(payload: ILoginSuccessPayload): ILoginSuccess {
  return {
    type: ActionTypes.LOGIN_SUCCESS,
    payload
  };
}

export function loginError(): ILoginError {
  return {
    type: ActionTypes.LOGIN_ERROR
  };
}
