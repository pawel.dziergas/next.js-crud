import { call, put, takeLatest } from 'redux-saga/effects';
import Router from 'next/router';
import { ILoginData } from './types/models';
import apiClient from '../../../utils/api/apiClient';
import { Endpoints, Routes } from '../../../config';
import * as actions from './actions';
import { ILoginRequest, ActionTypes } from './types/actions';
import { setCookie } from '../../../utils/auth/cookies';

const loginRequest = async (formData: ILoginData): Promise<{}> => {
  const { data } = await apiClient().post(Endpoints.login, { ...formData });
  return data;
};

function* loginRequestSaga(action: ILoginRequest) {
  try {
    const { accessToken } = yield call(loginRequest, action.payload.values);
    const { email: userEmail } = action.payload.values;
    yield setCookie('token', accessToken);
    yield Router.push(Routes.HOME);
    yield put(
      actions.loginSuccess({
        userEmail
      })
    );
  } catch (error) {
    if (error.response?.data) {
      const { message, errors } = error.response.data;
      action.payload.actions.setErrors(errors);
      action.payload.actions.setStatus({ error: message });
    }

    yield put(actions.loginError());
  }
}

export default function* saga() {
  yield takeLatest(ActionTypes.LOGIN, loginRequestSaga);
}
