import { Actions, ActionTypes } from './types/actions';
import { ILoginState } from './types/state';
import { combineReducers } from 'redux';

export const initialState: ILoginState = {
  requesting: false,
  error: false,
  userEmail: ''
};

function requesting(state = initialState.requesting, action: Actions): boolean {
  switch (action.type) {
    case ActionTypes.LOGIN: {
      return true;
    }
    case ActionTypes.LOGIN_SUCCESS:
    case ActionTypes.LOGIN_ERROR: {
      return false;
    }
    default: {
      return state;
    }
  }
}

function error(state = initialState.error, action: Actions): boolean {
  switch (action.type) {
    case ActionTypes.LOGIN_ERROR: {
      return true;
    }
    case ActionTypes.LOGIN_SUCCESS: {
      return false;
    }
    default: {
      return state;
    }
  }
}

function userEmail(state = initialState.userEmail, action: Actions): string {
  switch (action.type) {
    case ActionTypes.LOGIN_ERROR: {
      return '';
    }
    case ActionTypes.LOGIN_SUCCESS: {
      return action.payload.userEmail;
    }
    default: {
      return state;
    }
  }
}

export default combineReducers<ILoginState, Actions>({
  requesting,
  error,
  userEmail
});
