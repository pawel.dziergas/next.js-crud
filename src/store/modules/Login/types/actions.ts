import { ILoginPayload, ILoginSuccessPayload } from './models';

export enum ActionTypes {
  LOGIN = '[LOGIN] login',
  LOGIN_SUCCESS = '[LOGIN] login - success',
  LOGIN_ERROR = '[LOGIN] login - error'
}

export interface ILoginRequest {
  readonly type: ActionTypes.LOGIN;
  payload: ILoginPayload;
}

export interface ILoginSuccess {
  readonly type: ActionTypes.LOGIN_SUCCESS;
  payload: ILoginSuccessPayload;
}

export interface ILoginError {
  readonly type: ActionTypes.LOGIN_ERROR;
}

export type Actions = ILoginRequest | ILoginSuccess | ILoginError;
