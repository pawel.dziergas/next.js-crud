import { FormikHelpers } from 'formik';

export interface ILoginPayload {
  values: ILoginData;
  actions: FormikHelpers<ILoginData>;
}

export interface ILoginData {
  email: string;
  password: string;
}

export interface ILoginSuccessPayload {
  userEmail: string;
}
