export interface ILoginState {
  requesting: boolean;
  error: boolean;
  userEmail: string;
}
