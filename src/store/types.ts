import { ILoginState } from '../store/modules/Login/types/state';
import { IContactsState } from '../store/modules/Contacts/types/state';

export interface IApplicationState {
  login: ILoginState;
  contacts: IContactsState;
}
