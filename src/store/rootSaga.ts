import { all } from 'redux-saga/effects';
import LoginSaga from '../store/modules/Login/saga';
import ContactsSaga from '../store/modules/Contacts/saga';

function* rootSaga() {
  yield all([LoginSaga(), ContactsSaga()]);
}

export default rootSaga;
