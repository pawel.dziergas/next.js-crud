import { combineReducers } from 'redux';
import loginReducer from '../store/modules/Login/reducers';
import contactsReducer from '../store/modules/Contacts/reducers';
import { IApplicationState } from './types';

export default combineReducers<IApplicationState>({
  login: loginReducer,
  contacts: contactsReducer
});
