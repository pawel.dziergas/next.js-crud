import React, { FC } from 'react';
import './styles.scss';
import { Container, Toolbar } from '@material-ui/core';

const Footer: FC<{}> = () => {
  return (
    <div className="footer">
      <Container>
        <Toolbar disableGutters={true}>
          <div className="footer__bottom-section">
            <p className="footer__text">All rights reserved by Contacts App © 2020</p>
          </div>
        </Toolbar>
      </Container>
    </div>
  );
};

export default Footer;
