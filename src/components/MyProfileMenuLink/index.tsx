import React, { FC } from 'react';
import LinkComponent from '../Link';
import {
  Button,
  Popper,
  Grow,
  Paper,
  ClickAwayListener,
  MenuList,
  MenuItem,
  makeStyles,
  Avatar
} from '@material-ui/core';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import './styles.scss';
import useProfileNav from './hooks/useProfileNav';

const useStyles = makeStyles(() => ({
  avatar: {
    width: '30px',
    height: '30px'
  },
  button: {
    marginRight: '10px',
    color: '#7f8c8d',
    fontWeight: 400,
    textTransform: 'none',
    fontSize: '16px'
  },
  dropdownMenu: {
    zIndex: 1000
  }
}));

const MyProfileMenuLink: FC<{}> = () => {
  const classes = useStyles();
  const { anchorRef, isOpen, handleToggle, userEmail, handleClose, handleLogout } = useProfileNav();

  return (
    <div className="my-profile-menu-link">
      <Button
        className={classes.button}
        ref={anchorRef}
        aria-controls={isOpen ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
      >
        <Avatar className={classes.avatar} />
        {userEmail && <span className="my-profile-menu-link__user-name"></span>}
      </Button>
      <Popper
        open={isOpen}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        className={classes.dropdownMenu}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList autoFocusItem={isOpen} id="menu-list-grow">
                  <MenuItem onClick={handleClose}>
                    <LinkComponent text="Log out" href="" onClick={handleLogout} icon={PowerSettingsNewIcon} />
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
};

export default MyProfileMenuLink;
