import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { getloggedUserEmail } from '../../../store/modules/Login/selectors';
import { destroyCookie } from '../../../utils/auth/cookies';
import Router from 'next/router';
import { Routes } from '../../../config';

const useProfileNav = () => {
  const [isOpen, setOpen] = useState(false);
  const anchorRef = useRef<HTMLButtonElement>(null);
  const userEmail = useSelector(getloggedUserEmail);

  const handleLogout = () => {
    destroyCookie('token');
    Router.push(Routes.LOGIN);
  };

  const handleToggle = () => {
    setOpen(prevOpen => !prevOpen);
  };

  const handleClose = (event: ChangeEvent<any>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const prevOpen = useRef(isOpen);
  useEffect(() => {
    if (prevOpen.current && !isOpen) {
      anchorRef?.current?.focus();
    }

    prevOpen.current = isOpen;
  }, [isOpen]);

  return {
    anchorRef,
    isOpen,
    handleLogout,
    handleClose,
    handleToggle,
    userEmail
  };
};

export default useProfileNav;
