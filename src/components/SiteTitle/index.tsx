import Link from 'next/link';
import React, { FC } from 'react';

import { Routes } from '../../config';

import './styles.scss';

const SiteTitle: FC<{}> = () => {
  return (
    <Link href={Routes.HOME}>
      <a className="site-title">
        <h1 className="site-title__text">
          Contacts <span>app</span>
        </h1>
      </a>
    </Link>
  );
};

export default SiteTitle;
