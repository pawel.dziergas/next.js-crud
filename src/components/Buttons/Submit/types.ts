export interface ISubmitBtnProps {
  text: string;
  disabled?: boolean;
  loading?: boolean;
  requesting?: boolean;
}
