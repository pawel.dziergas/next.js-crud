import Button from '@material-ui/core/Button';
import React, { FC } from 'react';

import { ISubmitBtnProps } from './types';
import { CircularProgress } from '@material-ui/core';

const SubmitBtn: FC<ISubmitBtnProps> = ({ text, disabled, loading, requesting }) => {
  return (
    <Button
      disabled={disabled || loading}
      className="button button--full-width"
      type="submit"
      size="large"
      variant="contained"
      color="primary"
    >
      {requesting ? <CircularProgress size={20} /> : text}
    </Button>
  );
};

export default SubmitBtn;
