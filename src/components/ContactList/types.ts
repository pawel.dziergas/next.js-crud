import { IContact } from '../../types/contact';
import { modalType } from '../../pages/home/useContactModals';

export interface IContactListProps {
  items: IContact[];
  actionCall: (id: string, actionType: modalType) => void;
}
