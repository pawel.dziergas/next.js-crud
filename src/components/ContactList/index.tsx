import React, { FC, Fragment, memo, PropsWithChildren } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { IContactListProps } from './types';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { modalTypes } from '../../pages/home/useContactModals';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  }
}));

const ContactList: FC<IContactListProps> = ({ items, actionCall }) => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      {items.map(({ id, name, email, phone, country }) => (
        <Fragment key={id}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar>{name.charAt(0).toUpperCase()}</Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={name}
              secondary={
                <>
                  <Typography component="span" variant="body2" className={classes.inline} color="textPrimary">
                    E-mail: {email}
                    <br /> Phone: {phone}
                  </Typography>
                  <br /> {country}
                </>
              }
            />
            <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="edit" onClick={() => actionCall(id, modalTypes.EDIT)}>
                <EditIcon />
              </IconButton>
              <IconButton edge="end" aria-label="delete" onClick={() => actionCall(id, modalTypes.REMOVE)}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <Divider variant="inset" component="li" />
        </Fragment>
      ))}
    </List>
  );
};

function areEqual(
  prev: Readonly<PropsWithChildren<IContactListProps>>,
  next: Readonly<PropsWithChildren<IContactListProps>>
) {
  const { items: prevItems } = prev;
  const { items } = next;

  return JSON.stringify(prevItems) === JSON.stringify(items);
}

export default memo(ContactList, areEqual);
