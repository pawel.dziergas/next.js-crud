export interface ILoaderProps {
  type?: 'linear' | 'circular';
}
