import React, { FC } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withStyles } from '@material-ui/core';
import { ILoaderProps } from './types';

const ColorCircularProgress = withStyles({
  root: {
    color: '#34495e'
  }
})(CircularProgress);

const ColorLinearProgress = withStyles({
  colorPrimary: {
    backgroundColor: '#34495e'
  },
  barColorPrimary: {
    backgroundColor: '#34495e'
  }
})(LinearProgress);

const Loader: FC<ILoaderProps> = ({ type = 'circular' }) => {
  return type === 'linear' ? <ColorLinearProgress /> : <ColorCircularProgress />;
};

export default Loader;
