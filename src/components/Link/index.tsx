import React, { FC } from 'react';
import Link from 'next/link';
import './styles.scss';
import { ILinkProps } from './types';

const LinkComponent: FC<ILinkProps> = ({ text, href, icon, onClick }) => {
  const Icon = icon;

  return (
    <div className="link-component">
      {Icon && <Icon className="link-component__icon" />}
      {onClick ? (
        <span onClick={onClick} className="link-component__link">
          {text}
        </span>
      ) : (
        <Link href={href}>
          <a className="link-component__link">{text}</a>
        </Link>
      )}
    </div>
  );
};

export default LinkComponent;
