import { UrlObject } from 'url';

export interface ILinkProps {
  text: string;
  href: string | UrlObject;
  icon?: React.ElementType;
  onClick?: () => void;
}
