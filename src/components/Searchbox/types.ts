export interface ISearchboxProps {
  placeholder: string;
  handleSearch: (input: string) => void;
}
