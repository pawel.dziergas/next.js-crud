import React, { FC, useState, ChangeEvent } from 'react';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import { Button, InputAdornment, Grid } from '@material-ui/core';
import './styles.scss';
import { ISearchboxProps } from './types';

const Searchbox: FC<ISearchboxProps> = ({ placeholder, handleSearch }) => {
  const [inputValue, setInputValue] = useState('');

  const handleValue = (e: ChangeEvent<any>) => {
    setInputValue(e.target.value);
  };

  return (
    <Grid container justify="center" className="searchbar__label">
      <TextField
        size="small"
        id="searchbar"
        className="searchbar__input"
        placeholder={placeholder}
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          )
        }}
        onChange={handleValue}
      />
      <Button
        size="small"
        variant="contained"
        color="primary"
        className="searchbar__button"
        startIcon={<SearchIcon />}
        onClick={() => handleSearch(inputValue)}
      >
        Search
      </Button>
    </Grid>
  );
};

export default Searchbox;
