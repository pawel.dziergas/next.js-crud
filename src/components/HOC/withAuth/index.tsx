import React, { Component } from 'react';

import { Routes } from '../../../config';
import { isLoggedIn } from '../../../utils/auth/token';
import redirect from '../../../utils/redirect';

const withAuth = <T extends object>(C: any) => {
  return class AuthComponent extends Component<T> {
    static async getInitialProps({ ...ctx }: any) {
      if (!isLoggedIn(ctx.ctx)) {
        redirect(ctx.ctx, Routes.LOGIN);
      } else {
        C.getInitialProps(ctx);
      }
    }

    render() {
      return <C {...this.props} />;
    }
  };
};

export default withAuth;
