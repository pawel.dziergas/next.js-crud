import React, { FC } from 'react';
import { Field } from 'formik';
import InputForm from '../InputForm';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import './styles.scss';
import { IInputRangeProps } from './types';

const InputRange: FC<IInputRangeProps> = ({ nameFrom, nameTo, labelFrom, labelTo, handleBlur }) => {
  return (
    <div className="input-range">
      <Field name={nameFrom} label={labelFrom} onFieldBlur={handleBlur} component={InputForm} />
      <div className="input-range__separator">
        <SyncAltIcon />
      </div>
      <Field name={nameTo} label={labelTo} onFieldBlur={handleBlur} component={InputForm} />
    </div>
  );
};

export default InputRange;
