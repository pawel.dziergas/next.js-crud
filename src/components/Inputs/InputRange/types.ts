export interface IInputRangeProps {
  nameFrom: string;
  nameTo: string;
  labelFrom: string;
  handleBlur: (event: React.FocusEvent<any>) => void;
  labelTo: string;
}
