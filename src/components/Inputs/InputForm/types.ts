import { FieldProps } from 'formik';
type CustomMaskFunctionType = (value: string, length: number) => string;

export interface IInputProps extends FieldProps {
  type: string;
  mask: CustomMaskFunctionType | string;
  label?: string;
  value?: string;
  autoComplete?: string;
  disabled?: boolean;
  min?: boolean;
  max?: boolean;
  placeholder?: string;
  required?: boolean;
  onFieldChange?: (value: string) => void;
  valuesToReplaceFromMask?: string;
  onFieldBlur?: (e: React.FocusEvent<any>) => void;
  multiline?: boolean;
  rows?: string;
}
