import React, { FC, useState, ChangeEvent, FocusEvent } from 'react';
import { getIn } from 'formik';
import c from 'classnames';
import InputMask from 'react-input-mask';
import TextField from '@material-ui/core/TextField';
import { IInputProps } from './types';
import { shortenText } from '../../../utils/shortenText';

const InputForm: FC<IInputProps> = ({
  field,
  form,
  label,
  disabled,
  type,
  autoComplete,
  mask,
  placeholder,
  required,
  min,
  max,
  onFieldChange,
  onFieldBlur,
  valuesToReplaceFromMask = '\\D',
  multiline = false,
  rows = '1'
}) => {
  const touched = getIn(form.touched, field.name);
  const error = getIn(form.errors, field.name);
  const [inputValue, setInputValue] = useState('');
  const [inputMaskMaxLength, setInputMaskMaxLength] = useState(0);
  const inputFieldClasses = c('input__field', { 'input__field--error': touched && error });

  const handleChange = (event: ChangeEvent<any>) => {
    event.persist();
    if (mask) {
      const replacedValue = event.target.value.replace(new RegExp(valuesToReplaceFromMask, 'g'), '');

      onFieldChange?.(replacedValue);

      if (inputValue === replacedValue) {
        setInputMaskMaxLength(replacedValue.length + 1);
      } else {
        setInputMaskMaxLength(replacedValue.length);
      }
      setInputValue(replacedValue);
    } else {
      field.onChange(event);
    }
  };

  const handleBlur = (event: FocusEvent<any>) => {
    field.onBlur(event);
    onFieldBlur?.(event);
  };

  const labelText = (required ? `${label}*` : label) || '';

  return (
    <div className="input">
      {!multiline ? (
        <InputMask
          {...field}
          onChange={handleChange}
          onBlur={handleBlur}
          alwaysShowMask={false}
          mask={typeof mask === 'function' ? mask(inputValue, inputMaskMaxLength) : mask}
          disabled={disabled}
        >
          {() => (
            <TextField
              {...field}
              onChange={handleChange}
              id={field.name}
              label={
                <>
                  <span className="input__label-xs-up">{labelText}</span>{' '}
                  <span className="input__label-xs-down">{shortenText(labelText, 11)}</span>
                </>
              }
              name={field.name}
              className={inputFieldClasses}
              autoComplete={autoComplete}
              placeholder={placeholder}
              onBlur={handleBlur}
              type={type}
              fullWidth={true}
              inputProps={{ min, max, 'data-testid': `${field.name}-input` }}
              variant="outlined"
            />
          )}
        </InputMask>
      ) : (
        <TextField
          {...field}
          onChange={handleChange}
          id={field.name}
          label={required ? `${label}*` : label}
          name={field.name}
          className={inputFieldClasses}
          autoComplete={autoComplete}
          placeholder={placeholder}
          onBlur={handleBlur}
          type={type}
          inputProps={{ min, max }}
          variant="outlined"
          multiline={multiline}
          rows={rows}
        />
      )}
      {touched && error && (
        <p className="error-message error-message--input" data-testid="input-validation-error">
          {error}
        </p>
      )}
    </div>
  );
};

InputForm.defaultProps = {
  disabled: false,
  type: 'text',
  mask: ''
};

export default InputForm;
