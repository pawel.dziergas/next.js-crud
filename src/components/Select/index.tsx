import React, { FC, ChangeEvent } from 'react';
import { TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { ISelectProps } from './types';
import { Nullable } from '../../types/nullable';
import { ISelectItemProps } from '../../constants/offers';
import { getIn } from 'formik';

const Select: FC<ISelectProps> = ({
  field,
  form,
  label,
  options,
  value,
  onChange,
  disabled = false,
  variant = 'standard',
  disableClearable = false
}) => {
  const touched = getIn(form.touched, field.name);
  const error = getIn(form.errors, field.name);

  const showError = !!error && touched;

  return (
    <>
      <Autocomplete
        disabled={disabled}
        onChange={(event: ChangeEvent<any>, value: Nullable<ISelectItemProps>) => {
          form.setFieldTouched(field.name);
          onChange(value?.value);
        }}
        disableClearable={disableClearable}
        options={options || []}
        value={options?.find((option: any) => option.value == value) || null}
        getOptionLabel={(option: any) => option.label || ''}
        style={{ width: '100%' }}
        renderInput={params => (
          <TextField {...params} label={showError ? error : label} variant={variant} error={showError} />
        )}
      />
    </>
  );
};

export default Select;
