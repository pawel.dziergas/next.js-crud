import { ISelectItemProps } from '../../constants/offers';
import { FieldProps } from 'formik';

export interface ISelectProps extends FieldProps {
  onChange: (value?: string | number) => void;
  label: string;
  options?: ISelectItemProps[];
  variant?: any;
  disabled?: boolean;
  disableClearable?: boolean;
  value?: string | number | string[];
}
