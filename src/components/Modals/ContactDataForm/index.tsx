import React, { FC } from 'react';
import { BaseModal } from '..';
import './styles.scss';
import { IContactDataFormProps } from './types';
import ContactForm from '../../ContactDataForm';

const ContactDataForm: FC<IContactDataFormProps> = ({
  isOpen = true,
  text,
  header,
  handleClose,
  onSubmit,
  submitText = 'Add',
  initialValues
}) => {
  return (
    <BaseModal isOpen={isOpen} handleClose={handleClose} header={header}>
      <div className="contact-data-modal">
        <p className="contact-data-modal__text">{text}</p>
        <ContactForm
          onSubmit={(values, actions) => {
            onSubmit(values, actions);
          }}
          requesting={false}
          submitText={submitText}
          initialValues={initialValues}
        />
      </div>
    </BaseModal>
  );
};

export default ContactDataForm;
