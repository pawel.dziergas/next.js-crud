import { IProps } from '../../ContactDataForm/types';

export interface IContactDataFormProps {
  isOpen?: boolean;
  handleClose: () => void;
  header: string;
  text: string;
  onSubmit: IProps['onSubmit'];
  submitText?: string;
  initialValues?: IProps['initialValues'];
}
