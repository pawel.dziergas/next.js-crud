import React, { FC } from 'react';
import { BaseModal } from '..';
import './styles.scss';
import { IRemoveContactProps } from './types';
import Button from '@material-ui/core/Button';

const ContactRemoveModal: FC<IRemoveContactProps> = ({ isOpen = true, text, handleClose, onSubmit }) => {
  return (
    <BaseModal isOpen={isOpen} handleClose={handleClose} header="Remove confirmation">
      <div className="contact-data-modal">
        <p className="contact-data-modal__text">{text}</p>
        <Button className="button " type="submit" size="large" variant="contained" color="primary" onClick={onSubmit}>
          Yes, remove
        </Button>
      </div>
    </BaseModal>
  );
};

export default ContactRemoveModal;
