import { IProps } from '../../ContactDataForm/types';

export interface IRemoveContactProps {
  isOpen?: boolean;
  handleClose: () => void;
  text: string;
  onSubmit: () => void;
}
