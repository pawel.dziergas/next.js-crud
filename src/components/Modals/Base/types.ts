export interface IBaseModalProps {
  isOpen: boolean;
  handleClose: () => void;
  className?: string;
  header?: string;
}
