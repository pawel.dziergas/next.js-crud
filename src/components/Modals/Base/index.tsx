import React, { FC } from 'react';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CloseIcon from '@material-ui/icons/Close';
import './styles.scss';
import { IBaseModalProps } from './types';
import { Grid } from '@material-ui/core';

const BaseModal: FC<IBaseModalProps> = ({ isOpen, handleClose, className, header, children }) => {
  return (
    <Modal
      aria-labelledby="transition-modal-header"
      className={className}
      open={isOpen}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Fade in={isOpen}>
        <div className="base-modal">
          <button onClick={handleClose} className="base-modal__close-button button button--cleared">
            <CloseIcon />
          </button>
          {header && (
            <h2 id="transition-modal-title" className="base-modal__header">
              {header}
            </h2>
          )}
          <Grid container direction="column" alignItems="center">
            {children}
          </Grid>
        </div>
      </Fade>
    </Modal>
  );
};

export default BaseModal;
