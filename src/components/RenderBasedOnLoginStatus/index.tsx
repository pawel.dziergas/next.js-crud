import React, { FC } from 'react';

import { isLoggedIn } from '../../utils/auth/token';
import { IRenderBasedOnLoginStatusProps } from './types';

const RenderBasedOnLoginStatus: FC<IRenderBasedOnLoginStatusProps> = ({ authOn, children }) => {
  if (isLoggedIn() !== authOn) {
    return null;
  }
  return <>{children}</>;
};

RenderBasedOnLoginStatus.defaultProps = {
  authOn: true
};

export default RenderBasedOnLoginStatus;
