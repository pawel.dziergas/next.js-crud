import './styles.scss';
import { ICardProps } from './types';
import React, { FC } from 'react';

const Card: FC<ICardProps> = ({ children, width, height }) => {
  return (
    <div style={{ width, height }} className="card">
      {children}
    </div>
  );
};

Card.defaultProps = {
  width: '100%',
  height: '100%'
};

export default Card;
