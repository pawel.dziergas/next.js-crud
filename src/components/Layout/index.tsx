import React, { FC } from 'react';
import Head from 'next/head';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import RenderBasedOnLoginStatus from '../RenderBasedOnLoginStatus';
import './styles.scss';
import { ILayoutProps } from './types';
import { GeneralSettings } from '../../config';
import SiteTitle from '../SiteTitle';
import Footer from '../Footer';
import MyProfileMenuLink from '../MyProfileMenuLink';

const Layout: FC<ILayoutProps> = ({ children, title = '', titleWithPrefix = true, withContainer = true }) => {
  const _renderTitle = () => {
    const { separator, prefix } = GeneralSettings.title;
    let between = title ? separator : '';

    return titleWithPrefix ? prefix + between + title : title;
  };

  return (
    <div>
      <Head>
        <title>{_renderTitle()}</title>
      </Head>
      <div className="layout">
        <Container maxWidth="lg">
          <Toolbar disableGutters={true} className="layout__toolbar">
            <SiteTitle />
            <RenderBasedOnLoginStatus authOn={true}>
              <div className="layout__links">
                <MyProfileMenuLink></MyProfileMenuLink>
              </div>
            </RenderBasedOnLoginStatus>
          </Toolbar>
        </Container>
      </div>
      <div className="layout__content">{withContainer ? <Container>{children}</Container> : children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
