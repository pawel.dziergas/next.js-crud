import { ReactNode } from 'react';

export interface ILayoutProps {
  title?: string;
  children: ReactNode;
  titleWithPrefix?: boolean;
  withContainer?: boolean;
}
