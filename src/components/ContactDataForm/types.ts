import { FormikHelpers } from 'formik';

export interface IProps {
  onSubmit: (values: IContactDataFormValues, actions: FormikHelpers<IContactDataFormValues>) => void;
  initialValues?: IContactDataFormValues;
  submitText: string;
  requesting: boolean;
}

export interface IContactDataFormValues {
  name: string;
  email: string;
  phone: string;
  country: string;
}

export interface IContactFormPayload {
  values: IContactDataFormValues;
  actions: FormikHelpers<IContactDataFormValues>;
}
