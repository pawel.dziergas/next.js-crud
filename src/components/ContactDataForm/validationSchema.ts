import * as yup from 'yup';

const validationSchema = yup.object().shape<any>({
  name: yup
    .string()
    .required()
    .label('Name'),
  email: yup
    .string()
    .email()
    .required()
    .label('E-mail'),
  phone: yup
    .string()
    .required()
    .label('Phone'),
  country: yup
    .string()
    .required()
    .label('Country')
});

export default validationSchema;
