import React from 'react';
import { IContactDataFormValues, IProps } from './types';
import Input from '../Inputs/InputForm';
import { Grid } from '@material-ui/core';
import SubmitBtn from '../Buttons/Submit';
import { Formik, Form, Field } from 'formik';
import Select from '../../components/Select';
import validationSchema from './validationSchema';
import { countries, ISelectItemValue } from './countries';

const defaultValues: IContactDataFormValues = {
  name: '',
  email: '',
  phone: '',
  country: ''
};

const ContactDataForm = ({ onSubmit, initialValues = defaultValues, submitText, requesting }: IProps) => {
  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize={true}
    >
      {({ status, setFieldValue, values, dirty }) => (
        <Form>
          {status?.success && <p className="success-message success-message--modal">{status.success}</p>}
          <Grid container spacing={3} justify="flex-start" alignItems="flex-end">
            <Grid item xs={12}>
              <Field name="name" label={'Name'} component={Input} required />
            </Grid>
            <Grid item xs={12}>
              <Field name="email" label={'E-mail address'} component={Input} required />
            </Grid>
            <Grid item xs={12}>
              <Field name="phone" label={'Phone number'} component={Input} required />
            </Grid>

            <Grid item xs={12}>
              <Field
                name={'country'}
                label={'Country'}
                options={countries}
                value={values.country}
                onChange={(value: ISelectItemValue) => {
                  setFieldValue('country', value);
                }}
                component={Select}
              />
            </Grid>
            <Grid item xs={12}>
              <SubmitBtn disabled={requesting || !dirty} text={submitText} />
            </Grid>
          </Grid>
          {status?.error && <p className="error-message error-message--modal">{status.error}</p>}
        </Form>
      )}
    </Formik>
  );
};

export default ContactDataForm;
