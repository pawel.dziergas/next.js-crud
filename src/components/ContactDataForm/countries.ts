export type ISelectItemValue = string | number | undefined;

export interface ISelectItemProps {
  label: string;
  value: ISelectItemValue;
}

const countriesList = [
  'Albania',
  'Belarus',
  'Belgium',
  'Czech Republic',
  'Estonia',
  'Finland',
  'France',
  'Germany',
  'Greece',
  'Hungary',
  'Ireland',
  'Italy',
  'Latvia',
  'Mexico',
  'Netherlands',
  'Poland',
  'Portugal',
  'Romania',
  'Slovakia',
  'Slovenia',
  'Spain',
  'Sweden',
  'Turkey',
  'Ukraine',
  'United Kingdom',
  'United States'
];

export const countries: ISelectItemProps[] = countriesList.map(i => ({ label: i, value: i }));
